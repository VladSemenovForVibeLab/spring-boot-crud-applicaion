package com.semenov.crud.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.List;


@Getter
@Setter
@Entity
@Table(name = "personal_details")
@Schema(description = "Personal details",example = "1")
@NoArgsConstructor
@AllArgsConstructor
public class PersonalDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    @Schema(description = "user_id",example = "1")
    private Integer id;

    @Column(name = "first_name")
    @Schema(description = "Person firstname",example = "Vladislav")
    @NotNull(message = "Firstname must be not null")
    @Length(max = 255,message = "Firstname length must be smaller than 255 symbols")
    private String firstName;

    @Column(name = "last_name")
    @Schema(description = "Person lastname",example = "Semenov")
    @NotNull(message = "Lastname must be not null")
    @Length(max = 255,message = "Lastname length must be smaller than 255 symbols")
    private String lastName;

    @Column(name = "address")
    @Schema(description = "Person address",example = "Street Pushkin 5")
    @Length(max = 255,message = "Address length must be smaller than 255 symbols")
    @NotNull(message = "address must be not null")
    private String address;

    // Связь с задачами (OneToMany: один человек может иметь много задач)
    @OneToMany(mappedBy = "assignee",cascade = CascadeType.ALL)
    @Schema(description = "Person marks")
    private List<Marks> marks;
}