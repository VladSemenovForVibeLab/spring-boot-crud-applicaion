package com.semenov.crud.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "marks")
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Mark entity")
public class Marks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id", nullable = false)
    @Schema(description = "task_id",example = "1")
    private Integer id;

    @Column(name = "title")
    @NotNull(message = "title must be not null")
    @Length(max = 255,message = "Task title must be smaller then 255 symbols")
    private String title; // Заголовок задачи

    @Column(name = "description")
    @NotNull(message = "Task description must be not null")
    @Length(max = 255,message = "Task description must be smaller then 255 symbols")
    private String description; // Описание задачи

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "Due date must be not null")
    @Column(name = "due_date")
    private Date dueDate; // Срок выполнения задачи

    // Связь с ответственным человеком (ManyToOne: много задач принадлежит одному человеку)
    @ManyToOne
    @JoinColumn(name = "assignee_id")
    private PersonalDetails assignee;
}