package com.semenov.crud.controller;

import com.semenov.crud.model.PersonalDetails;
import com.semenov.crud.service.interface_package.CRUDService;
import com.semenov.crud.service.interface_package.PersonalDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/person-details")
public class PersonalDetailsController extends CRUDRestController<PersonalDetails,Integer> {
   private final PersonalDetailsService personalDetailsService;
    @Override
    CRUDService<PersonalDetails, Integer> getService() {
        return personalDetailsService;
    }
}
