package com.semenov.crud.controller;

import com.semenov.crud.model.Marks;
import com.semenov.crud.service.interface_package.CRUDService;
import com.semenov.crud.service.interface_package.MarksService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/marks")
public class MarksController extends CRUDRestController<Marks,Integer>{
    private final MarksService marksService;

    @Override
    CRUDService<Marks, Integer> getService() {
        return marksService;
    }
}
