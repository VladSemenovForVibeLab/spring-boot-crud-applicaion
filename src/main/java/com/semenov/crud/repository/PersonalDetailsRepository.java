package com.semenov.crud.repository;

import com.semenov.crud.model.PersonalDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonalDetailsRepository extends JpaRepository<PersonalDetails, Integer> {
    List<PersonalDetails> findByFirstName(String firstName);
}