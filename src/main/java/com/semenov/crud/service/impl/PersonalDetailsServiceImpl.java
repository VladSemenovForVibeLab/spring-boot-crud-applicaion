package com.semenov.crud.service.impl;

import com.semenov.crud.model.PersonalDetails;
import com.semenov.crud.repository.PersonalDetailsRepository;
import com.semenov.crud.service.interface_package.PersonalDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonalDetailsServiceImpl extends AbstractCRUDService<PersonalDetails,Integer> implements PersonalDetailsService {
   private final PersonalDetailsRepository personalDetailsRepository;
    @Override
    CrudRepository<PersonalDetails, Integer> getRepository() {
        return personalDetailsRepository;
    }
}
