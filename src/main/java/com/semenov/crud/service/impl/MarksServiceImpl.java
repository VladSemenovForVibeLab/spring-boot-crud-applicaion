package com.semenov.crud.service.impl;

import com.semenov.crud.model.Marks;
import com.semenov.crud.repository.MarksRepository;
import com.semenov.crud.service.interface_package.MarksService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarksServiceImpl extends AbstractCRUDService<Marks,Integer> implements MarksService {
    private final MarksRepository marksRepository;
    @Override
    CrudRepository<Marks, Integer> getRepository() {
        return marksRepository;
    }
}
