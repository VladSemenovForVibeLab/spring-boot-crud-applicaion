package com.semenov.crud.service.interface_package;

import com.semenov.crud.model.PersonalDetails;

public interface PersonalDetailsService extends CRUDService<PersonalDetails,Integer> {
}
