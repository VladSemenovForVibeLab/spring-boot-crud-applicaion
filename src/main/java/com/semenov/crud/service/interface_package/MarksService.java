package com.semenov.crud.service.interface_package;

import com.semenov.crud.model.Marks;

public interface MarksService extends CRUDService<Marks,Integer> {
}
